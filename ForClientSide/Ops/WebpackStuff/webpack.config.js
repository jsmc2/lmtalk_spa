var nib = require('../NodeModules/node_modules/nib');
var path = require('path');
console.log(path);
module.exports = {
    entry: "./EntryForLive05.js",
    output: {
        path: __dirname,
        filename: "../../DeployPkg/Assets/Bundles/bundle_app.js"
    },
	resolve: {
        root: "/users/webperson/MyWebsites/Live05/ForClientSide/Ops/NodeModules/node_modules"
    },
	resolveLoader: {
        root: "/users/webperson/MyWebsites/Live05/ForClientSide/Ops/NodeModules/node_modules"
    },
    module: {
        loaders: [
            { test: /\.css$/i, loader: "style-loader" },
            { test: /\.css$/i, loader: "css-loader" },
            
            { test: /\.styl$/i, loader: "style-loader" },
            { test: /\.styl$/i, loader: "css-loader" },
            { test: /\.styl$/i, loader: "stylus-loader" },
            
            { test: /\.html$/i, loader: "html-loader" },
			{
				test: /\.js?$/,
				exclude: /(node_modules|bower_components)/,
				loader: "babel-loader",
				query: {
					loose: "all",
					optional: ['runtime' /*, 'asyncToGenerator'*/],
					stage: 0
				}
			}
        ]
    },
	stylus: {
	  sourcemap: true,
	  use: [nib()],
	  import: ['nib']
	}
};
