The repo lmtalk_spa contains server-side presentation tier code and client-side presentation tier code for generating the LmTalk app for end users (not bots).  Note since there was no need for server-side code for this project, that respective ForServerSide/ServerSideSrc folder is empty and only the ForClientSide folder is relevant.

The ForClientSide folder contains the following descendant folder.  Here a brief description about some of them:

-----------------------------
ForClientSide/DeployPkg

*  ForClientSide/DeployPkg contains the (built) code and assets for deployment outside of development, and envelopes what is sent to the browser.  Index.html is the file that is sent to the browser to load up the app.  It's worth a look. 

* ForClientSide/DeployPkg/Assets/Bundles contains bundled vendor css, bundled vendor js, and webpack bundled our authored application js css html all in one bundle (ideally would be sharded into 2 or 3 smaller bundles).  The aforementioned app bundle is not shown in this particular repo instance.

* ForClientSide/DeployPkg/Assets/[Documents,Media] are self explanatory.

* ForClientSide/DeployPkg/Player holds the index.html for the player app (separate from the primary SPA) and the mediator code to provide for communication between the player app and the SPA page.

-----------------------------
ForClientSide/DeployPkg/Ops contains under subfolders the node.js programs for transform the application sources - css preprocessing, babel transpiling, etc - and packaging that it a bundle (that is place ForClientSide/DeployPkg/Assets/Bundles

-----------------------------
ForClientSide/DeployPkg/Ops contains under subfolders the bower.json references to third party resources, and some of the non-bowered full resources themselves.

-----------------------------
ForClientSide/DeployPkg/Source contains under subfolders our code, which consumed by webpack to build a the app bundle.

ForClientSide/DeployPkg/Source/Common/Utilities contains under subfolders non app specific boilerplate code.  

ForClientSide/DeployPkg/Source/Specific contains under subfolders code written specific for the app, including that for architectural constructs.  Perhaps the architectural constructs might be better located in a sibling folder.

ForClientSide/DeployPkg/Source/Specific/action_directives contains the Ng directives for adding functionality/behavior to tags, but they don't defined components.  Of note is ClickToMediatorAdc

ForClientSide/DeployPkg/Source/Specific/agents contains the REST API services and other utility services.  The word "agent" is used because they can be Ng services or Ng factories, etc.   Note the REST services are specific 

ForClientSide/DeployPkg/Source/Specific/veiwfilters contains Ng view filters - as methods organized under classes.

ForClientSide/DeployPkg/Source/Specific/vcomps contains the definitions for the (usually) visual ideas - 'visual components" on the page.  
   *** NOTE*** 
THESE VISUAL COMPONENTS (MINUS A FEW EXCEPTIONS) AS WELL AS THE REST SERVICES (PERHAPS MINUS A FEW EXCEPTIONS) UNDER ForClientSide/DeployPkg/Source/Specific/agents ARE THE ONLY THINGS THAT ARE SPECIFIC TO THE BUSINESS IDEA OF LM-TALK.
















